import argparse
import os
import re
import shutil
from PIL import Image

# This needs Pillow, which on Windows you can get from http://www.lfd.uci.edu/~gohlke/pythonlibs/#pillow. Download from the web, and run the installer.

def moveAndResize(sourceFile, destinationFile, maxSize):
    shutil.copy2(src, dest)
    im = Image.open(dest)

    size = im.size
    if (size[0] > size[1]):
        # Width greather than height
        factor = size[0] / maxSize
        size = (maxSize, int(size[1] / factor + 0.5))
    else:
        # Height greather than width
        factor = size[1] / maxSize
        size = (int(size[0] / factor), maxSize)

    imResized = im.resize(size)

    imResized.save(dest, "JPEG")

    im.close()
    imResized.close()

# Parse program arguments
parser = argparse.ArgumentParser()
parser.add_argument("inDir", help="Input directory where to search for files")
parser.add_argument("outDir", help="Output directory where to copy the files")
parser.add_argument("-p", "--prefix", default="Prefix", help="Prefix to add to all file names")
parser.add_argument("-d", "--dimension", type=int, default=800, help="Maximum X or Y dimension.")
parser.add_argument("-s", "--size", choices=["gallery"], help="Predefined sizes for different website purposes. Overrides --dimension")
args = parser.parse_args()

# It will always have a value because there is a default
maxDimension = args.dimension
if (args.size):
    if (args.size == "gallery"):
        maxDimension = 800

# Normalize the directories
in_path = os.path.abspath(args.inDir)
out_path = os.path.abspath(args.outDir)

# Find image files in the input directory and store them
index = 1
p = re.compile('.*\.(jpg|jpeg)$', re.IGNORECASE)
for file in os.listdir(in_path):
	if p.match(file):
		src = os.path.join(in_path, file)
		dest = os.path.join(out_path, args.prefix + str(index).zfill(2) + ".jpg")
		moveAndResize(src, dest, maxDimension)
		index = index + 1