import argparse
import os
import re
import shutil
from PIL import Image

# This needs Pillow, which on Windows you can get from http://www.lfd.uci.edu/~gohlke/pythonlibs/#pillow. Download from the web, and run the installer.

def moveAndResize(sourceFile, destinationFile, maxSize):
    shutil.copy2(src, dest)
    im = Image.open(dest)

    size = im.size
    if (size[0] > size[1]):
        # Width greather than height
        factor = size[0] / maxSize
        size = (maxSize, int(size[1] / factor + 0.5))
    else:
        # Height greather than width
        factor = size[1] / maxSize
        size = (int(size[0] / factor), maxSize)

    imResized = im.resize(size)

    imResized.save(dest, "JPEG")

    im.close()
    imResized.close()

# Parse program arguments
parser = argparse.ArgumentParser()
parser.add_argument("inFile", help="Input file to use")
parser.add_argument("-s", "--suffix", default="-Thumb", help="Suffix to add to generate the output file name")
args = parser.parse_args()

# Normalize the directories
inFile = os.path.abspath(args.inFile)
fileNameComponents = os.path.splitext(inFile)
outFile = fileNameComponents[0] + args.suffix + fileNameComponents[1]

# Create the new file
im = Image.open(inFile)

size = im.size
width = size[0]
height = size[1]
imResized = 0
if (width / height > 1.489):
    # Width much greater than the height, so shrink to the correct
    # height, then crop the width
    factor = height / 194
    size = ((int)(width / factor + 0.5), 194)
    imResized = im.resize(size)
elif (width / height > 1):
    # Width is not much greater than height, so shrink to the correct
    # width, then crop the height
    factor = width / 289
    size = (289, (int)(height / factor + 0.5))
    imResized = im.resize(size)
else:
    # Height greather than width
    raise Exception("The width must be greater than the height")

imResizedFinal = imResized.crop((0, 0, 289, 194))
imResizedFinal.save(outFile, "JPEG")

im.close()
imResized.close()
imResizedFinal.close()